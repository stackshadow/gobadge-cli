# run it with nix-shell build/shell.nix

{ pkgs ? import <nixpkgs> { } }:
let
  lib = pkgs.lib;
  inherit (lib) sourceByRegex;


  gobadge = import ./build/nix/gobadge-cli.nix { };

  # we need a newer version for gocyclo
  nixosGoCyclo = import
    (pkgs.fetchzip {
      url = "https://github.com/NixOS/nixpkgs/archive/bf01537f0c9deccf7906b51e101d05c039390bb8.zip";
      sha256 = "sha256-fgPiS1heTNSi5i+22pMxoj7t/iOg42zRZJqxeTCJPjU=";
    })
    { };
  gocyclo = nixosGoCyclo.gocyclo;

in
pkgs.mkShell {
  buildInputs = with pkgs;
    [
      go
      gocyclo
      gosec
      gobadge
      freefont_ttf
    ];
  shellHook = ''
    clean() {
      rm -v *.out.gobadge
      rm -v *.svg
    }

    cover() {
      export FONT_FILE=${pkgs.freefont_ttf}/share/fonts/truetype/FreeSans.ttf

      go test -v -timeout 60s -parallel 1 -coverprofile=coverage.out.gobadge  $(go list ./... | grep -v /mocked)
      go tool cover -func coverage.out.gobadge
      value=$(go tool cover -func coverage.out.gobadge | grep total: | awk '{print $3}' | sed 's/%//g')
      echo $value
      gobadge-cli --value-min=0.01 --value-max=100.00 --label=coverage --value=$value --file-name=./coverage.svg
    }
 
    cyclo() {
      export FONT_FILE=${pkgs.freefont_ttf}/share/fonts/truetype/FreeSans.ttf

      gocyclo -avg -ignore vendor . > cyclo.out.gobadge
      value=$(cat cyclo.out.gobadge | grep Average | cut -d':' -f2 | sed -e 's/\s*//')
      gobadge-cli --value-min=10.0 --value-max=0.01 --label=gocylco --value=$value --file-name=./gocyclo.svg
    }

    lastbuild(){
      export FONT_FILE=${pkgs.freefont_ttf}/share/fonts/truetype/FreeSans.ttf

      value=$(date +%d.%m.%Y)
      gobadge-cli --label=lastbuild --text --value=$value --file-name=./lastbuild.svg
    }

    gosec() {
      export FONT_FILE=${pkgs.freefont_ttf}/share/fonts/truetype/FreeSans.ttf

      gosec -color=false -no-fail -severity medium ./... > gosec.out.gobadge
      value=$(cat gosec.out.gobadge | grep Issues | cut -d':' -f2 | sed -e 's/\s*//')

      gobadge-cli --value-min=0.01 --value-max=100.00 --label=gosec --value=$value --file-name=./gosec.svg
    }
  


 
  '';
}
