package main

import (
	"errors"
	"fmt"
	"io/ioutil"
	"strconv"

	"os"

	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"gitlab.com/stackshadow/gobadge-cli/internal/gobadge/cli"

	"pkg.re/essentialkaos/go-badge.v1"
)

var Version string = "local"

func main() {

	err := DoIt()

	if err != nil {
		log.Error().Err(err).Send()
		os.Exit(-1)
	}
}

func calculatePercent(min, max, value float64) (floatPercent float64) {

	floatDiff := (max - min)

	floatPercent = value - min
	floatPercent = floatPercent * 100.0 / floatDiff

	if floatPercent < 0.0 {
		floatPercent = 0.0
	}

	log.Debug().
		Float64("min", min).
		Float64("max", max).
		Float64("value", value).
		Float64("calculated", floatPercent).Send()

	return
}

func DoIt() (err error) {

	log.Logger = log.Output(zerolog.ConsoleWriter{
		PartsOrder: []string{
			zerolog.TimestampFieldName,
			zerolog.LevelFieldName,
			zerolog.CallerFieldName,
			zerolog.MessageFieldName,
		},
		Out: os.Stderr,
	})

	cli.Parse()

	if cli.Parameter.Version {
		fmt.Print(Version)
		os.Exit(0)
	}

	// check if local file exist
	_, err = os.Stat("./font")
	if os.IsNotExist(err) {
		// check if font file exist
		_, err = os.Stat(cli.Parameter.FontFile)
	} else {
		log.Debug().Msg("load font from ./font")
		cli.Parameter.FontFile = "./font"
	}

	// parse float in non-text-mode
	var floatvalue, floatPercent float64
	if err == nil && !cli.Parameter.Text {
		floatvalue, err = strconv.ParseFloat(cli.Parameter.Value, 64)
	}

	// calculate percent
	if err == nil && !cli.Parameter.Text {
		floatPercent = calculatePercent(cli.Parameter.ValueMin, cli.Parameter.ValueMax, floatvalue)
	}

	// calculate color if needed
	if err == nil && cli.Parameter.Color == "" && !cli.Parameter.Text {
		// red
		cli.Parameter.Color = "#e05d44"

		// orange
		if floatPercent > 25.0 {
			cli.Parameter.Color = "#fe7d37"
		}

		// yellow
		if floatPercent > 50.0 {
			cli.Parameter.Color = "#dfb317"
		}

		// green
		if floatPercent > 75.0 {
			cli.Parameter.Color = "#97ca00"
		}
	}

	if cli.Parameter.FileName == "" {
		err = errors.New("no output file provided, can not create badge")
	}

	var generator *badge.Generator
	if err == nil {
		generator, err = badge.NewGenerator(cli.Parameter.FontFile, 11)
	}

	if err == nil {
		badgeData := generator.GeneratePlastic(cli.Parameter.Label, cli.Parameter.Value, cli.Parameter.Color)
		ioutil.WriteFile(cli.Parameter.FileName, badgeData, 0700)
	}

	return
}

// go test ./... -coverprofile cover.out
// go tool cover -func cover.out
