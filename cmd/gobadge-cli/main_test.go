package main

import (
	"os"
	"testing"
)

func TestHappyPath(t *testing.T) {
	os.Args = []string{
		"--label=integrationtest",
		"--value=10.5",
		"--file-name=./integrationtest.svg",
	}
	err := DoIt()
	if err != nil {
		t.FailNow()
	}
}

func TestWhitespacedText(t *testing.T) {
	os.Args = []string{
		"--label=A text with whitespaces",
		"--value=10.5",
		"--file-name=./integrationtest.svg",
	}
	err := DoIt()
	if err != nil {
		t.FailNow()
	}
}

func TestComplexity(t *testing.T) {
	os.Args = []string{
		"--label=integrationtest",
		"--value-min=5.0", // red
		"--value-max=0.0", // green
		"--value=4.5",
		"--file-name=./integrationtest.svg",
	}
	err := DoIt()
	if err != nil {
		t.FailNow()
	}
}

func TestWrongFloat(t *testing.T) {
	os.Args = []string{
		"--value=NotAFloat",
		"--file-name=./integrationtest.svg",
	}
	err := DoIt()
	if err == nil {
		t.FailNow()
	}
}

func TestTextMode(t *testing.T) {
	os.Args = []string{
		"--text",
		"--value", "A small text node",
		"--file-name=./integrationtest.svg",
	}
	err := DoIt()
	if err == nil {
		t.FailNow()
	}
}

func TestCalculation(t *testing.T) {
	var percent float64
	percent = calculatePercent(0.0, 10.0, 2.5)
	if percent != 25.0 {
		t.FailNow()
	}

	percent = calculatePercent(100.0, 0.0, 10.0)
	if percent != 90.0 {
		t.FailNow()
	}

	percent = calculatePercent(5.0, 0.0, 1.0)
	if percent != 80.0 {
		t.FailNow()
	}

}
