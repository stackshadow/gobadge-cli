# gobadge-cli

Status: Active

## Description
Create SVG-badges

## Installation

### Dependencies

- a font that can be used, we use FreeSans per default

## Usage

gobadge by default, treat your value as percent. 
The color for the badge is calculate from these value.
- \< 25% = red
- \> 25% = orange
- \> 50% = yellow
- \> 75% = green

For example
```
gobadge --label=success --value=85.0 --file-name ../../static/success_85.svg
```

gives you:

![Success badge](static/success_85.svg "Scuccess 85%")


Its also possible to scale the value, for example if you count security issues, 0 is good and 5 is really bad, you can do it like that:
```
gobadge --label="SecurityIssues" \
		--value-min=5.0 \
		--value-max=0.001 \
		--value=4.5 \
        --file-name ../../static/security_4.svg
```
![Success badge](static/security_4.svg "Scuccess 85%")


WARNING: Please be aware, that if `0.0` is used, that this will trigger the default value. For example the `value-max` defaults to `100.0`, so if you use `0.0`, `100.0` will be used. Thats why we use `0.001` here  


## Contributing

We are open for contributions, but please only contribute in GitLab.


## License
MIT

