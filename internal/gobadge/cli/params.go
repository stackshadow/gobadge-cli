package cli

import "github.com/alecthomas/kong"

type SParams struct {
	Text     bool    `help:"-value will be used without calculation"`
	FontFile string  `help:"The path to an ttf file that will be used for the badge" env:"FONT_FILE"`
	Value    string  `env:"VALUE"`
	ValueMin float64 `help:"The min value, where the color get red" default:"0.0" env:"VALUE_MIN"`
	ValueMax float64 `help:"The max value, where the color get green" default:"100.0" env:"VALUE_MAX"`
	Color    string  `help:"The color as hmtl-hash, this overwrite color detection" env:"COLOR" default:""`
	Label    string  `help:"The label for this badge" env:"LABEL"`
	FileName string  `env:"FILENAME"`
	Version  bool    `help:"if set, just print the version"`
}

var Parameter SParams

func Parse() {

	ctxKong := kong.Parse(&Parameter,
		kong.Name("gobadge"),
		kong.Description("Badge creation in go"),
		kong.UsageOnError(),
		kong.ConfigureHelp(kong.HelpOptions{
			Compact: false,
		}))

	ctxKong.ApplyDefaults()
	ctxKong.Bind(Parameter)

}
