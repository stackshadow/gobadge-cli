{ system ? builtins.currentSystem
, pkgs ? import <nixpkgs> { inherit system; }
}:
let
  lib = pkgs.lib;
  callPackage = lib.callPackageWith (pkgs);
  inherit (lib) sourceByRegex;

  # gobadge
  gobadge = callPackage ./build/nix/gobadge.nix {
    version = "1.5.0";
    vendorSha256 = null;
  };

in
{

  # we provide the package function, so you can choose a version in your external nix-files
  package = gobadge;


} 

