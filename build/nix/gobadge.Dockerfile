############################################## ARCH ##############################################
# syntax=docker/dockerfile
FROM nixos/nix:latest AS base

# The nix-channel
RUN nix-channel --add https://github.com/NixOS/nixpkgs/archive/062a0c5437b68f950b081bbfc8a699d57a4ee026.tar.gz nixpkgs && \
    nix-channel --update

# this enable kvm inside docker, that is needed to build an nix-docker-image
RUN echo "system-features = nixos-test benchmark big-parallel kvm" >> /etc/nix/nix.conf && \
    nix-env -iA nixpkgs.qemu_kvm


COPY ./ /usr/src/gobadge/

RUN cd /usr/src/gobadge && \
    nix-env -i $(nix-build build/nix/gobadge.nix)

# we add some additional tools
RUN nix-env -iA nixpkgs.docker nixpkgs.gnumake nixpkgs.go nixpkgs.gcc nixpkgs.gnused nixpkgs.gawk nixpkgs.gocyclo nixpkgs.gosec && \
    nix-env --delete-generations +1 && \
    nix-collect-garbage -d

WORKDIR /usr/src/gobadge
#ENTRYPOINT ["nix-env", "-S", "default" ]
