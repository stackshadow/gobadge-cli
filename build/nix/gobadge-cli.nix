{ system ? builtins.currentSystem
, version ? "local"
, vendorSha256 ? null
, homepage ? "https://gitlab.actaport.de/actaport/infrastructure/watchcat/"
}:
let
  pkgs = import <nixpkgs> { inherit system; };
  lib = pkgs.lib;
  inherit (pkgs) buildGoModule;
  inherit (lib) sourceByRegex maintainers;
in
buildGoModule {
  pname = "gobadge-cli";
  inherit version;
  src = sourceByRegex ./../.. [
    "^go.mod"
    "^go.sum"
    "cmd"
    "cmd/.*"
    "internal"
    "internal/.*"
    "vendor"
    "vendor/.*"
    "build"
    "build/local"
    "build/local/.*"
  ];


  excludedPackages = "test";
  subPackages = [ "cmd/gobadge-cli" ];

  inherit vendorSha256;
  proxyVendor = true;

  buildFlagsArray = ''
    -ldflags=
    -w -s
    -X main.Version=${version}
    -X main.Commit=${version}
    -extldflags=-static
  '';

  preBuild = ''
    export CGO_ENABLED=0
    export CFLAGS="-I${pkgs.glibc.dev}/include";
    export LDFLAGS="-L${pkgs.glibc}/lib";
  '';

  preCheck = ''
    export FONT_FILE=${pkgs.freefont_ttf}/share/fonts/truetype/FreeSans.ttf
  '';


  #passthru.tests = { inherit (nixosTests) telegraf; };

  meta = with lib;
    {
      description = "gobadge-cli - go based cli to create badges";
      license = licenses.mit;
      inherit homepage;
      maintainers = with maintainers; [ stackshadow ];
    };
}
