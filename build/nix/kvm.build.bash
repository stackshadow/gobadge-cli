#!/bin/bash
set +e

DOCKER_IMAGE=nixdockerbuilder:latest

exist=$(docker image inspect --format="ignore me" $DOCKER_IMAGE >/dev/null 2>&1 && echo -e yes || echo -e no )
if [ "$exist" == "no" ]; then
    echo "Build docker image"
    docker-compose -p nix -f build/nix/docker-compose.yml build
fi

# start
docker-compose -p nix -f build/nix/docker-compose.yml up --build -d

# build the docker-image
docker-compose -p nix -f build/nix/docker-compose.yml exec builder nix-build /usr/src/build/nix/gobadge.docker.nix