{ system ? builtins.currentSystem, pkgs ? import <nixpkgs> { inherit system; }
, lib ? pkgs.lib, go, gobadge, freefont_ttf, packages ? [ ], extraPackages ? [ ], name, src
, script ? ''value="0.0"'', label ? "gosec", secMin ? "0.001", secMax ? "100.0"
, isText ? false }:
let

in pkgs.stdenv.mkDerivation {
  inherit name;
  inherit src;

  buildInputs = [ go gobadge ] ++ packages ++ extraPackages;
  buildPhase = ''
    export GOCACHE="/tmp$(mktemp)"
    echo "GOCACHE: $GOCACHE"
    mkdir -vp $GOCACHE

    export GOPATH="/tmp$(mktemp)"
    echo "GOPATH: $GOPATH"
    mkdir -vp $GOPATH

    export FONT_FILE=${freefont_ttf}/share/fonts/truetype/FreeSans.ttf

  '' + script + ''

    ${gobadge}/bin/gobadge \
    --label=${label} \
    --value-min=${secMin} --value-max=${secMax} \
    ${lib.optionalString (isText == true) "--text"} \
    --value="$value" --file-name=badge.svg
  '';

  installPhase = ''
    mkdir -p $out/badges
    cp badge.svg $out/badges/${label}.svg
  '';
}

