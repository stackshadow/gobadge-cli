############################################## ARCH ##############################################
# syntax=docker/dockerfile
FROM nixos/nix:latest AS base

# The nix-channel
RUN nix-channel --add https://github.com/NixOS/nixpkgs/archive/a4d921be21135b13ce6c775d08d4bf6f5df37528.tar.gz nixpkgs && \
    nix-channel --update

# this enable kvm inside docker, that is needed to build an nix-docker-image
RUN echo "system-features = nixos-test benchmark big-parallel kvm" >> /etc/nix/nix.conf && \
    nix-env -iA nixpkgs.qemu_kvm
