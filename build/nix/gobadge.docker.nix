{ system ? builtins.currentSystem }:
let
  pkgs = import <nixpkgs> { inherit system; };
  lib = pkgs.lib;
  inherit (pkgs) dockerTools;
  inherit (lib) sourceByRegex;

  gobadge = import ./gobadge.nix { };
in
dockerTools.buildImage {
  name = "gobadge";
  tag = "dev";

  contents = with pkgs; [
    cacert
    gobadge
  ];

  runAsRoot = ''
    mkdir -p /tmp
    chmod a+rwX /tmp
  '';

  config = {
    Env = [
      "HOME=/tmp"
    ];

  };
}
