{ system ? builtins.currentSystem
, version ? "local"
, vendorSha256 ? null
, homepage ? "https://gitlab.actaport.de/actaport/infrastructure/watchcat/"
, repourl ? "git@gitlab.actaport.de:actaport/infrastructure/watchcat.git"
}:
let
  pkgs = import <nixpkgs> { inherit system; };
  lib = pkgs.lib;
  inherit (pkgs) buildGoModule fetchFromGitHub;
  inherit (lib) sourceByRegex;

  binary = import ./gobadge-cli.nix { };

  gobadge-clean = pkgs.writers.writeBash "/bin/gobadge-clean" ''
    export PATH="${binary}/bin:$PATH"
    make -f ${binary}/var/gobadge/badge-gocover.mak clean
    make -f ${binary}/var/gobadge/badge-gocyclo.mak clean
    make -f ${binary}/var/gobadge/badge-gosec.mak clean
    make -f ${binary}/var/gobadge/badge-lastbuild.mak clean
  '';

  gobadge-coverage = pkgs.writers.writeBash "/bin/gobadge-gocover" ''
    export PATH="${binary}/bin:$PATH"
    export PATH="${pkgs.go}/share/go/bin:$PATH"
    export PATH="${pkgs.gcc}/bin:$PATH"
    export PATH="${pkgs.gnused}/bin:$PATH"
    export PATH="${pkgs.gawk}/bin:$PATH"

    export FONT_FILE=${pkgs.freefont_ttf}/share/fonts/truetype/FreeSans.ttf
    make -f ${binary}/var/gobadge/badge-gocover.mak badge
  '';

  gobadge-gocyclo = pkgs.writers.writeBash "/bin/gobadge-gocyclo" ''
    export PATH="${binary}/bin:$PATH"
    export PATH="${pkgs.gocyclo}/bin:$PATH"
    export PATH="${pkgs.gnused}/bin:$PATH"

    export FONT_FILE=${pkgs.freefont_ttf}/share/fonts/truetype/FreeSans.ttf
    make -f ${binary}/var/gobadge/badge-gocyclo.mak badge
  '';

  gobadge-gosec = pkgs.writers.writeBash "/bin/gobadge-gosec" ''
    export PATH="${binary}/bin:$PATH"
    export PATH="${pkgs.gosec}/bin:$PATH"
    export PATH="${pkgs.gnused}/bin:$PATH"

    export FONT_FILE=${pkgs.freefont_ttf}/share/fonts/truetype/FreeSans.ttf
    make -f ${binary}/var/gobadge/badge-gosec.mak badge
  '';

  gobadge-lastbuild = pkgs.writers.writeBash "/bin/gobadge-lastbuild" ''
    export PATH="${binary}/bin:$PATH"

    export FONT_FILE=${pkgs.freefont_ttf}/share/fonts/truetype/FreeSans.ttf
    make -f ${binary}/var/gobadge/badge-lastbuild.mak badge
  '';


in
pkgs.symlinkJoin {
  name = "gobadge";

  shellHook =
    ''
      export PATH=${gobadge-clean}/bin:$PATH
      export PATH=${gobadge-coverage}/bin:$PATH
      export PATH=${gobadge-gocyclo}/bin:$PATH
      export PATH=${gobadge-gosec}/bin:$PATH
      export PATH=${gobadge-lastbuild}/bin:$PATH
    '';


  paths = [
    binary
    gobadge-clean
    gobadge-coverage
    gobadge-gocyclo
    gobadge-gosec
    gobadge-lastbuild
  ];
}
