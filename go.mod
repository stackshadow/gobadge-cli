module gitlab.com/stackshadow/gobadge-cli

go 1.16

require (
	github.com/alecthomas/kong v0.2.16
	github.com/golang/freetype v0.0.0-20170609003504-e2365dfdc4a0 // indirect
	github.com/kr/pretty v0.2.0 // indirect
	github.com/rs/zerolog v1.21.0
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/image v0.0.0-20190802002840-cff245a6509b // indirect
	pkg.re/essentialkaos/check.v1 v1.0.0 // indirect
	pkg.re/essentialkaos/go-badge.v1 v1.3.0
)
